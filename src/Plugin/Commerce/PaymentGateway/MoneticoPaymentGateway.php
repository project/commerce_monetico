<?php

namespace Drupal\commerce_monetico\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the PaymentStandard payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_monetico",
 *   label = "Monetico",
 *   display_label = "Monetico",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_monetico\PluginForm\MoneticoPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 * )
 */
class MoneticoPaymentGateway extends OffsitePaymentGatewayBase {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version number'),
      '#description' => $this->t('The number of the version of the payment kit.'),
      '#default_value' => isset($this->configuration['version']) ? $this->configuration['version'] : '3.0',
      '#required' => TRUE,
    ];

    $form['tpe'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TPE number'),
      '#description' => $this->t('The TPE number of your Monetico account on 7 characters (eg. 1234567).'),
      '#default_value' => isset($this->configuration['tpe']) ? $this->configuration['tpe'] : '',
      '#required' => TRUE,
    ];

    $form['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company number'),
      '#description' => $this->t('The company number of your Monetico account.'),
      '#default_value' => isset($this->configuration['company']) ? $this->configuration['company'] : '',
      '#required' => TRUE,
    ];

    $form['security_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Security key'),
      '#description' => $this->t('The security key based on 40 characters.'),
      '#default_value' => isset($this->configuration['security_key']) ? $this->configuration['security_key'] : '',
      '#required' => TRUE,
    ];

    $form['bank_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Bank type'),
      '#description' => $this->t('The bank type. This will define the URLs.'),
      '#options' => array(
        'cm' => $this->t('Crédit Mutuel'),
        'cic' => $this->t('CIC'),
        'obc' => $this->t('OBC'),
        'monetico' => $this->t('Monetico')
      ),
      '#default_value' => isset($this->configuration['bank_type']) ? $this->configuration['bank_type'] : '',
      '#required' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['bank_type'] = $values['bank_type'];
      $this->configuration['version'] = $values['version'];
      $this->configuration['security_key'] = $values['security_key'];
      $this->configuration['tpe'] = $values['tpe'];
      $this->configuration['company'] = $values['company'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $reference = $request->query->get('reference');
    $reference_array = explode('-', $reference);
    if (empty($reference_array)) {
      return;
    }
    $order_id = reset($reference_array);
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultipleByOrder($order);

    if (empty($payments)) {
      return;
    }
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = reset($payments);

    $code_return = $request->query->get('code-retour');
    switch ($code_return) {
      case 'payetest':
      case 'paiement':
        $payment->setState('completed');
        break;

      case 'Annulation':
        $payment->setState('authorization_voided');
        break;
    }

    $payment->save();
  }
}
