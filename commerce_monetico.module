<?php

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_monetico_commerce_payment_method_info() {
  $payment_methods = array();

  $display_title = theme('monetico_icons_title');
  $icons = theme('monetico_icons_payment');
  $display_title .= '<div class="commerce-monetico-icons"><span class="label">' . t('Includes:') . '</span>' . implode(' ', $icons) . '</div>';

  $payment_methods['commerce_monetico'] = array(
    'base' => 'commerce_monetico',
    'title' => t('Credit Mutuel - CIC'),
    'short_title' => t('Monetico'),
    'display_title' => $display_title,
    'description' => t('Monetico Payments'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: adds a message and CSS to the submission form.
 */
function commerce_monetico_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form['commerce_monetico_information'] = array(
    '#markup' => '<span class="commerce-commerce-monetico-info">' . t('(Continue with checkout to complete payment via Monetico.)') . '</span>',
  );

  return $form;
}

/**
 * Implements hook_theme().
 */
function commerce_monetico_theme() {
  return array(
    'monetico_icons_title' => array(
      'variables' => array(),
    ),
    'monetico_icons_payment' => array(
      'variables' => array(),
    ),
  );
}

/**
 * Theme function for "monetico_icons_title".
 *
 * @param array $variables
 *   Store data parameters.
 *
 * @return string
 *   The output of this theme.
 */
function theme_monetico_icons_title(&$variables) {
  $icon_variables = array(
    'path' => \Drupal::service('extension.list.module')->getPath('commerce_monetico') . '/kit/images/logocmcicpaiement.png',
    'title' => t('Monetico Payment'),
    'alt' => t('Monetico Payment'),
    'attributes' => array(
      'class' => array('commerce-monetico-icon'),
    ),
  );
  $icon = theme('image', $icon_variables);
  $display_title = t('!logo <b>Credit card Payment</b>', array('!logo' => $icon));
  return $display_title;
}

/**
 * Theme function for "monetico_icons_payment".
 *
 * @param array $variables
 *   Store data parameters.
 *
 * @return string
 *   The output of this theme.
 */
function theme_monetico_icons_payment(&$variables) {
  $icons = array();

  $payment_methods = array(
    'visa' => t('Visa'),
    'mastercard' => t('Mastercard'),
    'amex' => t('American Express'),
    'cb' => t('CB'),
  );

  foreach ($payment_methods as $name => $title) {
    $variables = array(
      'path' =>  \Drupal::service('extension.list.module')->getPath('commerce_monetico') . '/kit/images/' . $name . '.gif',
      'title' => $title,
      'alt' => $title,
      'attributes' => array(
        'class' => array('commerce-monetico-icon'),
      ),
    );
    $icons[$name] = theme('image', $variables);
  }

  return $icons;
}
